var express = require('express');
var router = express.Router();
var app = express();
const port = 27017;

/* GET home page. */
router.get('/',  (req, res, next)  => {
  res.render('index', { title: 'Express' }); 
});

router.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
}); 
 
const MongoClient = require('mongodb').MongoClient;
const url = `mongodb://localhost:${port}/hangmanDB`;
let firstQueryResult = [],
    queryResultGot = [],
    queryResultDev = [],
    onlyNameHint = {};

(async () => {
  try {
    const client = await MongoClient.connect(url,{ useNewUrlParser: true });
    const db = client.db("hangmanDB");
    const query = {short_category : 'got'};
    const query2 = {short_category : 'dev' };

    //sort: 1 asc -1 desc
    db.collection('exercises').find(query).sort({ name: 1 }).toArray((err, result) => {
      if (err) throw err;
      console.log(result);
      for(let res of result){
        console.log(res.name + '=> ' + res.hint)
      }
      queryResultGot.push(result);
    });
    
    db.collection('exercises').find(query2).sort({ name: 1 }).toArray((err, result) => {
      if (err) throw err;
      console.log(result);
      for(let res of result){
        console.log(res.name + '=> ' + res.hint)
      }
      queryResultDev.push(result);
    });
    db.collection('exercises').find().sort({name: -1}).limit(7).toArray((err, result) => {
      if (err) throw err;
      console.log(result);
      for(let res of result){
        console.log(res.name + '=> ' + res.hint)
      }
      firstQueryResult.push(result);
    });
    db.collection('exercises').find().sort({name: 1}).limit(7).toArray((err, result) => {
      if (err) throw err;
      console.log(result);
      for(let res of result){
        onlyNameHint[res.name] = res.hint;
      }
    });
    // db.close();
    client.close();
  } catch(e) {
    console.error(e)
  }
})()

router.get("/exercisesfromall", (req, res) => {
  res.send(firstQueryResult);
});

router.get("/exercisesgot", (req, res) => {
  res.send(queryResultGot);
});

router.get("/exercisesdev", (req, res) => {
  res.send(queryResultDev);
});

router.get("/exercisnamehintpair", (req, res) => {
  res.send(onlyNameHint);
});


module.exports = router;

//  npm run dev
