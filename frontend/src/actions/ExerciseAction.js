import axios from 'axios';

export const FETCH_EXERCISES_BEGIN   = 'FETCH_EXERCISES_BEGIN';
export const FETCH_EXERCISES_SUCCESS = 'FETCH_EXERCISES_SUCCESS';
export const FETCH_EXERCISES_FAILURE = 'FETCH_EXERCISES_FAILURE';

export const GUESS_A_LETTER = 'GUESS_LETTER';

export default alphabet => {
  return {
    type: GUESS_A_LETTER,
    payload: alphabet
  }
}

export const fetchExercisesBegin = () => ({
  type: FETCH_EXERCISES_BEGIN
});

export function fetchExercisesSuccess(exercises) {
 // console.log(exercises);
  return {
    type: FETCH_EXERCISES_SUCCESS,
    payload: exercises
  };
}

export const fetchExercisesFailure = error => ({
  type: FETCH_EXERCISES_FAILURE,
  payload: { error }
});

const URL = 'http://localhost:7000/';
const DEFAULT_QUERY = 'exercisnamehintpair';

const config = {
  method: 'get',
  url: URL + DEFAULT_QUERY,
  withCredentials: false,
  headers: {'Content-Type': 'application/x-www-form-urlencoded'},
  responseType: 'json'
}

export function fetchExercises() {
    return dispatch => {
      dispatch(fetchExercisesBegin());
        return axios.request(config)
            .then(res => {
            dispatch(fetchExercisesSuccess(res.data));
            return res.data[0];
            }).catch(error => dispatch(fetchExercisesFailure(error)));
    };
}
  
  // Handle HTTP errors since fetch won't.
  function handleErrors(response) {
    if (!response.ok) {
      throw Error(response.statusText);
    }
    return response;
  }