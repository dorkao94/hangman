import React, { Component } from 'react';
import './App.css';
import HangmanMain from './components/HangmanMain';
import HandleAPI from './HandleAPI';

class App extends Component {
  constructor(props) {
    super(props);  
  }
 
  render() {
    return (
      <div className="App">
          {/* <HandleAPI /> */}

          <HangmanMain  />
      </div>
    );
  }
}

export default App;