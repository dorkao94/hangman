import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import './Man.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import { Provider } from 'react-redux';
import store from './store/store';
// import { fetchExercises } from '../src/actions/ExerciseAction.js';

// store.dispatch(fetchExercises());
ReactDOM.render(
<Provider store={store}>
    <App />
</Provider>,
document.getElementById('root'));
registerServiceWorker();
