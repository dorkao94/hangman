import {
  GUESS_A_LETTER,
} from '../actions/ExerciseAction';

const words = [
    'arya stark',
    'mongodb',
    'joyeuse erenford',
    'jon snow',
];
let word = words[Math.floor(Math.random() * words.length)];
let array = [];
for (let i = 0; i < word.length; i++) {
  array.push('_');
}

let format = array.join(' ')

let initialState = {
  word,
  format,
  guesses: [],
  wrongGuessesCount: 0,
  status: 'player',
 }

function wrongGuessCount(word, guesses) {
   let failedGuess = []
   for (let i = 0; i < guesses.length; ++i) {
     let wrongGuess = guesses[i];
     if (word.indexOf(guesses[i]) === -1) {
       failedGuess.push(wrongGuess);
     }
   }
   return failedGuess.length;
}

function showGuess(word, guesses) {
  var guessed = [];
  var letters = word.split("");
  for (let i = 0; i < letters.length; ++i) {
    if (guesses.indexOf(letters[i]) !== -1) {
      guessed.push(letters[i]);
    } else {
      guessed.push("_");
    }
  }
  return guessed.join(" ");
}

function isWinner(word, guesses) {
  var countWrong = wrongGuessCount(word, guesses);
  var secretWord = word.split("").join(" ");
  var correct = showGuess(word, guesses);
  if (countWrong < 6 && secretWord === correct) {
    return 'winner';
  } else if (countWrong < 6){
    return 'player';
  } else {
    return 'loser';
  }
}
export default function game ({word, format, guesses, wrongGuessesCount, status} = initialState, { type, payload } = {}, exercises) {
  switch(type) {
    case GUESS_A_LETTER:
      let newGuesses = guesses.concat([payload]);
      let newGuessCount = wrongGuessCount(word, newGuesses);
      let showLetter = showGuess(word, newGuesses);

      return {
        word,
        format: showLetter,
        guesses: newGuesses,
        wrongGuessesCount: newGuessCount,
        status: isWinner(word, newGuesses),
      }
    
    default :
      return {word, format, guesses, wrongGuessesCount, status}
  }
}
