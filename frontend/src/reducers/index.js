import game from './game';
import exercises from './exercises';

function conbineBoth(state = {}, action) {
  let a = game(state.game, action. exercises);
  let b = exercises(state.exercises, action); // note: b depends on a for computation
  return { a, b };
}
export default {
  game,
  exercises,
  conbineBoth
}
