import React, { Component } from 'react';
import { connect } from 'react-redux';
import ExerciseAction from '../actions/ExerciseAction';

import '../App.css';

const states = {
    abc: 'abcdefghijklmnopqrstuvwxyz'.split(''),
    usedLetters: [],
    lives: 10
};

class AlphabetButtons extends Component {
    constructor(props) {
      super(props); 
      this.state = states;
    }

    renderAlphabets(alphabet, index){
        return (
            <button ref="btn" className="AlphabetButton" id={'alphabet-'+alphabet} 
                    key={index} onClick={this.makeGuess.bind(this,alphabet)} disabled={this.props.status==='winner' || this.props.status ==='loser'}>
                    {alphabet}
            </button>
        );
    }

    makeGuess(alphabet) {
        if (typeof this.props.handleLives === 'function') {
            this.props.handleLives(this.props.lives)
        }

        this.props.ExerciseAction(alphabet);
        this.setState(prevState => ({
            usedLetters: [...prevState.usedLetters, alphabet]
          }));

    }

    render() {
      return (
        <div>
            {this.state.abc.map(this.renderAlphabets.bind(this))}
        </div>
      );
    }
  }


function mapStateToProps(state) {
    return {
      exercises: state.exercises.items,
      status: state.game.status
    }
}
export default connect(mapStateToProps, { ExerciseAction })(AlphabetButtons);
