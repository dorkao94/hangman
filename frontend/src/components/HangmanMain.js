import React, { Component } from 'react';
import '../App.css';
import AlphabetButtons from '../components/AlphabetButtons';
import GameOutput from './GameOutput';
import '../Man.css';


const stateObject = {
    abc: 'abcdefghijklmnopqrstuvwxyz'.split(''),
    categories: ['got', 'dev'],
    lives: 10, 
    toggle: true,
    resetDisable: false,
    displayOption: 'transparentFirst',
    showHint: false
};

class HangmanMain extends Component {
  constructor(props) {
    super(props);

    this.state = stateObject;
    this.eventHandler = this.eventHandler.bind(this);
    this.handleLives = this.handleLives.bind(this);

  }

  handleLives(){
    this.setState({lives: this.state.lives -1 >=0 ? this.state.lives -1 : this.state.lives})
   }


  handleHintClick(){
    this.setState({showHint: true})
    if (this.state.displayOption === 'transparentFirst') {
      this.setState({displayOption: 'nonTransparentFirst'});
    } else {
      this.setState({displayOption: 'transparentFirst'});
    }
  }

  handlePlayagainClick(){
    window.location.reload()
  }

  eventHandler(event) {
    if(event.target.id == 'chosenCategory0'){
      this.setState( {
          toggle: false
      })
    }else  {
      this.setState( {
        toggle: true 
      })
    } 
  }

  shouldComponentUpdate(nextProps, nextState) {
    return this.state.toggle !== nextState.toggle || this.state.lives !== nextState.lives || 
    this.state.displayOption !== nextState.displayOption || this.state.showHint !== nextState.showHint
    || this.state.resetDisable !== nextState.resetDisable;
  }
 
  render() {
    return (
      <div className="HangmanMain">
        <h1> Awesome Hangman game (:</h1>  
      
        <p>Use the alphabet below to guess the word, or click hint to get a clue.</p>  
        <div className="buttons">
          <AlphabetButtons lives={this.state.lives} handleLives={this.handleLives}/>
        </div>  
        <GameOutput showHint={this.state.showHint}/>
        {/* <p >You have {this.state.lives} lives</p> */}
        {/* <p>{this.state.lives === 0 ? 'Game over. Try again.' : 'Still got chances.'}</p> */}
        
        <button className="commonBtn hintButton" onClick={() => this.handleHintClick()}>Hint</button>
        <button className="commonBtn playagainButton" onClick={() => this.handlePlayagainClick()}> Play again</button>

      </div>
    );
  }
}

export default HangmanMain;