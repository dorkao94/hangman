import React, {Component } from 'react';
import { connect } from 'react-redux';
import { fetchExercises } from "../actions/ExerciseAction";


class GameOutput extends Component {
  constructor(props) {
    super(props);     
    this.state = {
      exercisesName : "",
      hintClicked :false
    };
    this.pickRandomProperty = this.pickRandomProperty.bind(this);
  }

  componentWillMount() {
    this.props.dispatch(fetchExercises());

  }

  componentWillUpdate(nextProps) {
    if (nextProps.showHint !== this.props.showHint) {
    }
  }

  pickRandomProperty(obj) {
    let result;
    let count = 0;
    for (let prop in obj){
        if (Math.random() < 1/++count)
           result = prop;
    }
    this.state.exercisesName = result;
    console.log(result)
    let array = [];
    for (let i = 0; i < result.length; i++) {
      if(result[i] == ' '){
        array.push(' ');
      }else{
        array.push('_');
      }
    }

    let format = array.join(' ')
    this.props.game.format = format;
    this.state.hintClicked = true;
    return obj[result];
  }

  render() {
    return (
      <div> 
      {/* <p>{this.props.exercises[this.props.game.word] != 'undefined' ? `Hint: ${this.props.exercises[this.props.game.word]}` : ''}</p> */}

      <h2>{this.props.showHint===true && this.state.hintClicked === false? 'Hint: '+this.pickRandomProperty(this.props.exercises) : ''}</h2>
      <div>   
        {
        Object.keys(this.props.exercises).map((key, index) => 
          <li key={index}>{key + '=> ' +this.props.exercises[key]}</li>
        )
        }
      </div>

         <p>Word to be guessed: {this.props.game.format}</p>
        <p> Wrong Guesses: { this.props.game.wrongGuessesCount }</p>
        <p>All letters: {this.props.game.guesses.join(', ')}</p>
        <p> Status: {this.props.game.status} </p> 
      </div>
    )
  }
}

 
function mapStateToProps(state) {
  console.log('state:');
  console.log(state);
  return {
    exercises: state.exercises.items,
    game: state.game
  }
}

export default connect(mapStateToProps)(GameOutput);

