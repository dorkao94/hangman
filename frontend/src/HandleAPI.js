import React, { Component } from 'react';
import axios from 'axios';
import './App.css';

const URL = 'http://localhost:7000/';
const DEFAULT_QUERY = 'exercisesfromall';

const config = {
  method: 'get',
  url: URL + DEFAULT_QUERY,
  withCredentials: false,
  headers: {'Content-Type': 'application/x-www-form-urlencoded'},
  responseType: 'json'
}

class HandleAPI extends Component {
    constructor(props) {
      super(props);
  
      this.state = {
        exercisesGot: [],
        exercisesDev: [],
        exercises: [],
        hints: [], 
        words: [],
        selected: '',
        category: this.props.category || "",
        random: 0,
        displayOption: 'transparentFirst'
      }; 
    }
    componentWillReceiveProps(nextProps){
      if (this.props.category == 'got' && nextProps.category == 'dev' || 
        this.props.category == 'dev' && nextProps.category == 'got' ) {
          this.setState({ category:nextProps.category });
          let exercises = this.state.category == 'got' ? this.state.exercisesGot : this.state.exercisesDev;
          this.setState({ exercises });
          this.setState({ random: Math.floor(0 + Math.random() * (this.state.exercises.length - 0)) })
      }
    }

    componentDidMount() {
      axios.request(config)
        .then(res => {
          const exercisesGot = res.data[0];
          const exercisesDev = res.data[1];
          this.setState({ exercisesGot, exercisesDev });
          let exercises = this.state.category == 'got' ? this.state.exercisesGot : this.state.exercisesDev;
          this.setState({ exercises });
          this.setState({ random: Math.floor(0 + Math.random() * (this.state.exercises.length - 0)) })
          this.setState({
            selected: this.state.exercises.map((exercise, i) => {i===this.state.random? exercise.name : ''}) 
          })

        }).catch(error => {
          console.log(error);
        });
    }

    shouldComponentUpdate(nextProps, nextState) {
      return this.state.displayOption !== nextProps.displayOption;
  }
 
    render() {
      return (
        <div>
            {this.state.exercises.map((exercise, i) => <li className={this.props.displayOption} key={i}> {i===this.state.random? exercise.hint : ''}</li>) }
            {this.props.random}     
        </div>
      );
    }
}

export default HandleAPI;